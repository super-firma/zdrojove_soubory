var number = 1;
var clickTimeout;
$(document).ready(function() {
  var hashTagActive = "";
  $("#scroll a").on("click touchstart" , function (event) {
      if(hashTagActive != this.hash) {
          event.preventDefault();
          var dest = 0;
          if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
              dest = $(document).height() - $(window).height();
          } else {
              dest = $(this.hash).offset().top;
          }
          $('html,body').animate({
              scrollTop: dest
          }, 800, 'swing');
          hashTagActive = this.hash;
      }
  });

  var searchOpen = 0;
  $("#search .fa").click(function() {
    if (searchOpen == 0) {
      $("#search .fa").removeClass("fa-search");
      $("#search .fa").addClass("fa-caret-up");
      searchOpen = 1;
    } else {
      $("#search .fa").addClass("fa-search");
      $("#search .fa").removeClass("fa-caret-up");
      searchOpen = 0;
    }
    $("#search-form").toggle(200);
  });

  var menuOpen = 0;
  $(window).resize(function() {
    if (menuOpen == 0) {
      if ($(window).width() > 1325) {
        $("#menu").css("display", "block");
      } else {
        $("#menu").css("display", "none");
      }
    }
  });
  $("#menu-button").click(function() {
    if (menuOpen == 0) {
      menuOpen = 1;
    } else {
      menuOpen = 0;
    }
    $("#menu").toggle(200);
  });

  $(".slid").css("opacity", 0);
  $("#slid" + number).css("opacity", 1);
  $(".slid-nav1").removeClass("active");
  $(".slid-nav" + number).addClass("active");

  $("#slider #kat .test").click(function() {
    number = $(this).text();
    clearTimeout(clickTimeout);
    clickTimeout = setTimeout(function(){
      changeSlid();
    }, 300);

    $("#kat li.test").removeClass("active");
    $(this).addClass("active");
  });

  $("#slider #kat .prev").click(function() {
    if (number < 2) {
      number = $("#slider .slid").length;
    } else {
      number--;
    }
    clearTimeout(clickTimeout);
    clickTimeout = setTimeout(function(){
      changeSlid();
    }, 300);
  });

  $("#slider #kat .next").click(function() {
    if (number < $("#slider .slid").length) {
      number++;
    } else {
      number = 1;
    }
    clearTimeout(clickTimeout);
    clickTimeout = setTimeout(function(){
      changeSlid();
    }, 300);
  });

  timeSlider();

  ;(function($, win) {
    $.fn.inViewport = function(cb) {
       return this.each(function(i,el){
         function visPx(){
           var H = $(this).height(),
               r = el.getBoundingClientRect(), t=r.top, b=r.bottom;
           return cb.call(el, Math.max(0, t>0? H-t : (b<H?b:H)));
         } visPx();
         $(win).on("resize scroll", visPx);
       });
    };
  }(jQuery, window));
  $(".animated").inViewport(function(px){
      if(px) $(this).addClass("animate") ;
  });
});

function changeSlid() {
  $(".slid").css("opacity", 1);
  $("#slid" + number).css("opacity", 2);
  $("#kat li.test").removeClass("active");
  $(".slid-nav" + number).addClass("active");

  $(".slid .content").animate({"opacity": "0"}, 300, "swing", function() {
    $("#slid" + number + " .content").animate({"opacity": "1"}, 300, "swing", function() {
    });
  });

  $(".slid .bg").css("z-index", 1);
  $("#slid" + number + " .bg").css("z-index", 2);
  $("#slid" + number + " .bg").animate({"opacity": "1"}, 600, "swing", function() {
    $(".slid .bg").css("opacity", 0);
    $("#slid" + number + " .bg").css("opacity", 1);
  });
}

function timeSlider() {
  if (number < $("#slider .slid").length) {
    number++;
  } else {
    number = 1;
  }
  changeSlid();
  window.setTimeout(function() { changeSlid() }, 8000);
}
