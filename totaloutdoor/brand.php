<div class="row" style="background-color:white;padding-top: 1%;padding-bottom: 2%;">
	<div class="container">
		<div class="col-sm-12 col-xs-12 visible-xs visible-sm ">
			<div class="top-bar">
				<div class="search-bar"><input class="search-input" placeholder="hledaný produkt" type="text" name="search" /><i class="fa fa-search hladat" aria-hidden="true"></i></div>
			</div>
		</div>
        <br/>
		<div class="col-md-12">
			<div class="hl-title">
				<h2><span>NAŠE ZNAČKY</span></h2>
			</div>
			<style>
                .col-md-2 {
                    width: 20%;
                    }
					
				@media (max-width: 450px){
					.znacky-ikonky-image{
						max-width:100%;
					}

					.vertical-align > [class^="col-"],
					.vertical-align > [class*=" col-"] {
					  display: inline-block;
						vertical-align: middle;
					}
					.search-bar{
						margin-top:10%;
						margin-bottom:10%;
					}
				}
            </style>
			<div class="description-brand  hidden-sm hidden-xs">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
			<div class="row">
                <div class="col-md-12 hidden-xs hidden-sm">
				
					
                    <div class="znacky-ikonky-prvy row" >
                        <img class="znacky-ikonky-image" alt="beal" src="../assets/images/brand1.png" inactive="../assets/images/brand1.png" active="../assets/images/brand1_active.png" />
                        <img class="znacky-ikonky-image active" alt="gsi outdoors" src="../assets/images/brand2_active.png" inactive="../assets/images/brand2.png" active="../assets/images/brand2_active.png" />
                        <img class="znacky-ikonky-image" alt="steripen" src="../assets/images/brand3.png" inactive="../assets/images/brand3.png" active="../assets/images/brand3_active.png"/>
                        <img class="znacky-ikonky-image" alt="pieps" src="../assets/images/brand4.png" inactive="../assets/images/brand4.png" active="../assets/images/brand4_active.png" />
                        <img class="znacky-ikonky-image" alt="camp" src="../assets/images/brand5.png" inactive="../assets/images/brand5.png"  active="../assets/images/brand5_active.png" />
					</div>
				</div>
				
			</div>
		   <div class="row">
			   <div class="col-md-12  hidden-xs hidden-sm">
				   <div class="znacky-ikonky-druhy row" >
					   <img class="znacky-ikonky-image" alt="lifesystems" src="../assets/images/brand6.png" inactive="../assets/images/brand6.png" active="../assets/images/brand6_active.png"/>
					   <img class="znacky-ikonky-image" alt="lifeventure" src="../assets/images/brand7.png" inactive="../assets/images/brand7.png" active="../assets/images/brand7_active.png"/>
					   <img class="znacky-ikonky-image" alt="littlelife" src="../assets/images/brand8.png" inactive="../assets/images/brand8.png" active="../assets/images/brand8_active.png"/>
					   <img class="znacky-ikonky-image" alt="mountain paws" src="../assets/images/brand9.png" inactive="../assets/images/brand9.png" active="../assets/images/brand9_active.png" />
				   </div>
			   </div>
		   </div>
		</div>
		<div class="row visible-sm visible-xs vertical-align">
			<div class="col-sm-12 col-xs-12 text-center">
				<img class="znacky-ikonky-image" alt="camp" src="../assets/images/brand5.png" inactive="../assets/images/brand5.png"  active="../assets/images/brand5_active.png" />
			</div>
			<div class="clearfix visible-xs-block"></div>
			<div class="col-sm-6 col-xs-6 text-center">
				<img class="znacky-ikonky-image" alt="beal" src="../assets/images/brand1.png" inactive="../assets/images/brand1.png" active="../assets/images/brand1_active.png" />
			</div>
			<div class="col-sm-6 col-xs-6 text-center">
				<img class="znacky-ikonky-image active" alt="gsi outdoors" src="../assets/images/brand2_active.png" inactive="../assets/images/brand2.png" active="../assets/images/brand2_active.png" />
			</div>
			 <div class="clearfix visible-xs-block"></div>
			<div class="col-sm-6 col-xs-6 text-center">
				<img class="znacky-ikonky-image" alt="steripen" src="../assets/images/brand3.png" inactive="../assets/images/brand3.png" active="../assets/images/brand3_active.png"/>
			</div>
			<div class="col-sm-6 col-xs-6 text-center">
				<img class="znacky-ikonky-image" alt="pieps" src="../assets/images/brand4.png" inactive="../assets/images/brand4.png" active="../assets/images/brand4_active.png" />
			</div>
			 <div class="clearfix visible-xs-block"></div>
			
			<div class="col-sm-6 col-xs-6 text-center">
				<img class="znacky-ikonky-image" alt="lifesystems" src="../assets/images/brand6.png" inactive="../assets/images/brand6.png" active="../assets/images/brand6_active.png"/>
			</div>
			 
			<div class="col-sm-6 col-xs-6 text-center">				
			   <img class="znacky-ikonky-image" alt="lifeventure" src="../assets/images/brand7.png" inactive="../assets/images/brand7.png" active="../assets/images/brand7_active.png"/>
			</div>
			<div class="col-sm-6 col-xs-6 text-center">	   
			   <img class="znacky-ikonky-image" alt="littlelife" src="../assets/images/brand8.png" inactive="../assets/images/brand8.png" active="../assets/images/brand8_active.png"/>
			</div>
			 
			<div class="col-sm-6 col-xs-6 text-center">
			  <img class="znacky-ikonky-image" alt="mountain paws" src="../assets/images/brand9.png" inactive="../assets/images/brand9.png" active="../assets/images/brand9_active.png" />
			</div>
		</div>
		<br/>
	</div>
</div>
 
