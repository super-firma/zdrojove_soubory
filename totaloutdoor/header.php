<?php
    if ( $lang != "cz"){
        $lang = "sk";
    }


?>

<!DOCTYPE html>

<html>
<!-- head -->
<head>

<!-- meta -->
    
    <meta charset="utf-8" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1" />-->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/print.css" media="print">
	<link rel="shortcut icon" href="../favicon.ico" />
	<link rel="apple-touch-icon" href="../assets/images/apple-touch-icon-precomposed.png" />
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    
	<style>

        .product-tabs{
            margin-top:5% !important;
        }
        .product-tabs li a{
            background-color: white;
            border:2px solid #cdd515;
            color:black;
            margin-left:-1%;
            margin-right:-1%;
            -webkit-border-radius:0;
            -moz-border-radius:0;
            border-radius:0;
            display: block;
            text-align: center;
            padding: 4% 0;
        }
        .product-tabs li{
            width:25%;
        }
        .product-tabs li.active a, .product-tabs li.active:active a, .product-tabs li:hover a .product-tabs li.active:focus a{
            text-decoration: none !important;
            color:black !important;
            font-weight:bold !important;
            background-color:#cdd515 !important;
            border:2px solid #cdd515 !important;
        }
        @media (min-width: 450px){
			.mobile {
				display: none;
			}
			
        }
		@media (max-width: 450px){

			.btn-scroll-button {
				display: block !important;
				border: 1px solid #f39800 !important;
				background-color: transparent;
				color: #f39800 !important;
				margin-left: 0px;
				
			}
			nav.navigation.computer {
				display: none;
			}
			nav.navigation {
				margin-bottom: 0 !important;
				margin-top: 0 !important;
				padding-top: 27%;
			  
			}
			
			footer.footer{
				margin-top:0px;
			}
			.top-bar{
				padding-right:0;
				
			}
			.top-barCont{
				padding-bottom:20px;
			}
			
			.navbar-toggle{
				float:none !important;
				margin-right:0px !important;
			}
		}
    </style>
</head>

<!-- body -->
<body>
	<header>
		<div class="container">
			<div class="row">

				<div class="col-md-3 col-sm-12 col-xs-12 text-center">
					<div class="logo">
						<a target="_blank" href="../"><img src="../assets/images/logo.png" alt="logo" title="logo"/></a>
					</div>


				</div>

				<div class="col-md-12 mobile text-center">
					
					<nav class="navbar navbar-default">
					
						<div class="container-fluid">
						
							<div class="navbar-header">
							
								<button 
									type="button" 
									class="navbar-toggle collapsed" 
									data-toggle="collapse" 
									data-target="#navbarmobile" 
									aria-expanded="false" >
										<span class="sr-only">Toggle navigation</span>
										&nbsp <i class="fa fa-bars"></i> MENU &nbsp 
								</button>
							</div>
							<div class="collapse navbar-collapse" id="navbarmobile">
							
								<ul class="nav nav-stacked nav-justified">
									<li class="menu-item "><a target="_blank" class="" href="../kdo-jsme.php">kdo jsme ?</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../nase-znacky.php">naše značky</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../category.php">katalog produktu</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../prodejci.php">prodejci</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../blog.php">blog</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../contact.php">kontakt</a></li>
								</ul>
							</div>
						
							   <!--  <button class="btn btn-scroll-button"><i class="fa fa-bars"></i></button> 
								<ul class="menu-navbar">
									<li class="menu-item "><a target="_blank" class="" href="../kdo-jsme.php">kdo jsme ?</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../nase-znacky.php">naše značky</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../category.php">katalog produktu</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../prodejci.php">prodejci</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../blog.php">blog</a></li>
									<li class="menu-item "><a target="_blank" class="" href="../contact.php">kontakt</a></li>
								</ul>-->
						</div>
					</nav>
				</div>
				
				<div class="col-md-9 col-sm-9 col-xs-12 top-barCont text-center">

					<div class="top-bar">
						<div class="search-bar hidden-sm hidden-xs"><input class="search-input" placeholder="hledaný produkt" type="text" name="search" /><i class="fa fa-search hladat" aria-hidden="true"></i></div>
						<div class="pro-partnery"><a target="_blank" href="../pro-partnery.php">pro partnery</div>
						<div class="prihlaseni"><a target="_blank" href="../prihlaseni.php"><i class="fa fa-user" aria-hidden="true"></i>  přihlášení</a></div>
						<div class="cart"><a target="_blank" href="../cart.php"><img src="../assets/images/cart.png"/></a></div>
						<div class="language">

								<li class="cz-language <?php echo ($lang == 'cz')? 'active' :'' ?>"><a href="index.php">CZ</a></li>
								<li class="sk-language <?php echo ($lang == 'sk')? 'active' :'' ?>"><a href="index_sk.php">SK</a></li>
						</div>
					</div>
				</div>
			</div>
			<nav class="navigation computer">
				<button class="btn btn-scroll-button"><i class="fa fa-bars"></i></button>
				<ul class="menu-navbar">
					<li class="menu-item "><a target="_blank" class="" href="../kdo-jsme.php">kdo jsme ?</a></li>
					<li class="menu-item "><a target="_blank" class="" href="../nase-znacky.php">naše značky</a></li>
					<li class="menu-item "><a target="_blank" class="" href="../category.php">katalog produktu</a></li>
					<li class="menu-item "><a target="_blank" class="" href="../prodejci.php">prodejci</a></li>
					<li class="menu-item "><a target="_blank" class="" href="../blog.php">blog</a></li>
					<li class="menu-item "><a target="_blank" class="" href="../contact.php">kontakt</a></li>
				</ul>
			</nav>
		</div>
   	</header>
<script type="text/javascript" src="assets/js/anime.js"></script>
<script>
    $('.carousel').carousel({
        interval: 2000
    })

    $(document).ready(function()
    {
        $(".znacky-ikonky-image").click(function(){

            var header = $(this).attr('alt').toString().toUpperCase();

            $('.brand-title').fadeOut('fast').text(header).fadeIn('fast');

            $(".znacky-ikonky-image").removeClass('active');

            $(this).fadeOut('fast')

            $(".znacky-ikonky-image").each(function(index){

                $(this).attr('src',$(this).attr('inactive'));

            })

            $(this).addClass('active');
            $(this).attr('src',$(this).attr('active'));
            $('.center-image').attr('src',$(this).attr('active'))
            $(this).fadeIn('fast')



            return 0;
        })

        if( $(window).width() <= 450){

            var nav_bar = $(".navigation .menu-navbar");

            nav_bar.slideUp('fast');

            $('.btn-scroll-button .computer').click(function(){

                if( nav_bar.css('display') === 'none' ){

                    nav_bar.slideDown('fast')

                } else{

                    nav_bar.slideUp('fast')

                }

            })
        }
    });

</script>