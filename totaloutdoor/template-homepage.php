<div class="container-full">
	<div class="background">
		<style>
			@media (max-width: 450px){
				.homepage-image-container{
					max-height: 200px;
				}
			}
			@media (min-width: 451px){
				.rowx2{
					margin-left: 1%;
					padding-left: 1%;
				}
			}
		</style>
		<div class="container home">
			<div class="row" style="    margin-top: 2%;">
				<div class="col-md-4 col-xs-12 col-sm-12">
					<div class="homepage-image-container test-div" style="background-image: url('../assets/images/horolezectvi.png');height: 450px; margin-bottom: 2.5%;">
						<span class="description-homepage">
							horolezectví
						</span>
					</div>
					<div class="homepage-image-container test-div" style="background-image: url('../assets/images/dog-1557956_1920.jpg');height: 252px;">
						<span class="description-homepage">
							pro zvířata
						</span>
					</div>
				</div>
				<div class="col-md-8  col-xs-12 col-sm-12">
					<div class="homepage-image-container test-div" style="background-image:url('../assets/images/prace-ve-vyskach.png');height: 350px; ">
						<span class="description-homepage">
							práce ve výškach
						</span>
					</div>
					<div class="row rowx2" style="">
						<div class="col-md-5" style="padding-left: 0 !important;">
							<div class="homepage-image-container test-div" style="background-image: url('../assets/images/beautiful-2297215_1920.png');height: 350px;">
								<span class="description-homepage">
									detské vybavení
								</span>
							</div>
						</div>
						<div class="col-md-7">
							<div class="homepage-image-container test-div" style="background-image: url('../assets/images/camping.png');height: 350px;">
								<span class="description-homepage">
									camping
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
