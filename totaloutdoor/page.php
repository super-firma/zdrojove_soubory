<div class="container-full cart">
    <div class="hl-title">
        <h2><span>KOŠÍK</span></h2>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <div class="loader">
                    <div class="stage active-line">
                        <p class="circle"></p>
                        <a id="first_step" class="step_link">
                            <span class="text">košík</span>
                        </a>
                    </div>
                    <div class="stage with-line active-line active">
                        <p class="circle"></p>
                        <a id="second_step" class="step_link">
                            <span class="text">dodací údaje</span>
                        </a>
                    </div>
                    <div class="stage with-line">
                        <p class="circle"></p>
                        <a id="third_step" class="step_link">
                            <span class="text">doprava, platba</span>
                        </a>
                    </div>
                    <div class="stage with-line">
                        <p class="circle"></p>
                        <a id="fourth_step" class="step_link">
                            <span class="text">objednávka</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="first_step step">
            <div class="row">
                <div class="col-md-12">

                    <!-- cart -->

                    <div class="container">
                        <div class="cart">
                            <table>
                                <tr>
                                    <td style="width:150px">
                                        <img src="assets/images/produkt1.png" style="width:100%"/>
                                    </td>
                                    <td>
                                        <span class="product_name">Opera 8,6 urlcore</span>
                                        <br/>
                                        <span class="brand">Beal</span>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: flex;">
                                                <span class="input_description" style="width:50%;text-align:right;padding-right: 5%;padding-top: 2%;">Množství</span>
                                                <input type="number" class="form-control input" style="width:20%"/>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display:flex">
                                                <span class="input_description" style="width:50%;text-align:right;padding-right: 5%;padding-top: 2%;margin-left:4.5%">Dostupnost</span>
                                                <span class="input" style="width:50%;padding-top:2%">
                                                    <i class="fa fa-car"></i>&nbsp;není skladem
                                            </span>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width:20%">
                                        <p>
                                            <span class="description">Technologie: </span> <span class="content">adgadsfga</span>
                                        </p>
                                        <p>
                                            <span class="description">Technologie: </span> <span class="content">adgadsfga</span>
                                        </p>
                                        <p>
                                            <span class="description">Technologie: </span> <span class="content">adgadsfga</span>
                                        </p>
                                    </td>

                                    <td>
                                        <div class="row">

                                            <div class="price">
                                                <span class="price">1790 KČ</span>
                                                <br/>
                                                <span class="remove" style="color:black">odebrat</span>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:150px">

                                        <img src="assets/images/produkt1.png" style="width:100%"/>

                                    </td>
                                    <td>
                                        <span class="product_name">Opera 8,6 urlcore</span>
                                        <br/>
                                        <span class="brand">Beal</span>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: flex;">
                                                <span class="input_description" style="width:50%;text-align:right;padding-right: 5%;padding-top: 2%;">Množství</span>
                                                <input type="number" class="form-control input" style="width:20%"/>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display:flex">
                                                <span class="input_description" style="width:50%;text-align:right;padding-right: 5%;padding-top: 2%;margin-left:4.5%">Dostupnost</span>
                                                <span class="input" style="width:50%;padding-top:2%">
                                                <i class="fa fa-car"></i>&nbsp;není skladem
                                            </span>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width:20%">
                                        <p>
                                            <span class="description">Technologie: </span> <span class="content">adgadsfga</span>
                                        </p>
                                        <p>
                                            <span class="description">Technologie: </span> <span class="content">adgadsfga</span>
                                        </p>
                                        <p>
                                            <span class="description">Technologie: </span> <span class="content">adgadsfga</span>
                                        </p>
                                    </td>

                                    <td>
                                        <div class="row">

                                            <div class="price">
                                                <span class="price">1790 KČ</span>
                                                <br/>
                                                <span class="remove" style="color:black">odebrat</span>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                <br/>
                            </table>
                        </div>
                    </div>

                    <!-- /. cart -->

                    <!-- summary -->

                    <div class="container">
                        <table class="summary">
                            <tbody>
                            <tr>
                                <td>
                                <span class="mail">
                                    <i class="fa fa-envelope"></i>&nbsp;Poslat seznam košíku mailem
                                </span>
                                </td>
                                <td>
                                    <span style="color:black;font-weight: bold;font-size:15px">Dárkovy poukaz</span>
                                </td>
                                <td>
                                    <input type="text" class="form-control" placeholder="zadejte kod"
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <span style="color:black;font-weight: bold;font-size:15px">K úhrade</span>
                                </td>
                                <td>
                                    <span class="price">3580 KČ</span>
                                    <span class="subprice">3300 KČ bez DPH</span>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td>

                                    <div class="pridatdokosika " style="float:left"><a target="_blank" href="#">Zpátky k nakúpu</a></div>
                                </td>
                                <td>

                                    <div class="pridatdokosika active" style="float:right"><a target="_blank" href="#">Pokračovat</a></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="row jumbotron-cart">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <span class="content">Nakupte na našem eshope nad 500 KČ a méte od nás poštovné ZDARMA!</span>
                            </div>
                        </div>
                    </div>

                    <!-- /. summary -->

                    <!--related products-->


                    <div class="container">
                        <div class="col-md-12">
                            <div class="hl-title">
                                <h2 class="cart-header"><span>Souvisejici produkty</span></h2></div>
                            <div id="index-carousel" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner" role="listbox">

                                    <div class="item active">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item ">
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="product-item">
                                                <div class="image-box">
                                                    <img src="../assets/images/produkt1.png"/>
                                                </div>
                                                <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                                <div class="znacky">Outdoor</div>
                                                <div class="cena-produktu">98 765 Kč</div>
                                                <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#index-carousel" role="button" data-slide="prev">
                                    <img src="assets/images/green-left-arrow.png"/>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#index-carousel" role="button" data-slide="next">
                                    <img src="assets/images/green-right-arrow.png" />
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                            <br/>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <button class="btn btn-default more-products">
                                        Vice produktu
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>


                    <!--/. related products -->
                </div>
            </div>
        </div>
        <div class="second_step active_step step">

            <div class="row login-register">

            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8" style="padding:0 5%">
                    <div class="form">
                        <div class="without-registration-content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="title">
                                    <h3>Informace</h3>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <label>jméno*</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label>příjmení*</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label>emailova adresa*</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label>telefón*</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Nakupuji na firmu
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>firma</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label>IČO</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label>DIČ</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="title">
                                    <h3>Dodací adresa</h3>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <label>ulice+č.p*</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label>město*</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label>psč*</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Fakturační adresa je zhodná s dodací adresou
                                    </label>
                                </div>
                                <div class="title">
                                    <h3>
                                        Fakturační adresa
                                    </h3>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <label>ulice+č.p*</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label>město*</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label>psč*</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group note-to-order">
                                    <label>poznámka k objednávce</label>
                                    <textarea>

                                </textarea>
                                    <span class="note">
                                    *povinná položka
                                </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Souhlasím se spracovaním osobních údaju
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <button class="btn btn-default send-btn">Pokračovat na dopravu a platbu</button>
                            </div>
                        </div>
                        </div>
                        <div class="row login-register">
                            <div class="col-md-6 col-sm-6 col-xs-12 login-cart">
                                <div class="hl-title cart-title subtitle-cart">
                                    <h4>
                                        <span>
                                            Již jsem zde nakupoval
                                        </span>
                                    </h4>
                                </div>
                                <form>
                                    <div class="input-group">
                                        <label>přihlašovací meno *</label>
                                        <input type="text" class="form-control"/>
                                    </div>
                                    <div class="input-group">
                                        <label>heslo *</label>
                                        <input type="password" class="form-control"/>
                                    </div>
                                    <span class="legend">*povinná položka</span>
                                    <br/>
                                    <span class="forget-passwrd">
                                        Zapomňeli ste heslo? Kliknete <a href="cart.php">zde</a>.
                                    </span>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Zapamatovat si mě
                                        </label>
                                    </div>

                                    <div class="read-more-blog login-button" style="float: left"><a href="#">Přihlásit se</a></div>
                                    </form>
                                </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 register-cart">
                                <div class="hl-title cart-title subtitle-cart">
                                    <h4>
                                        <span>
                                            Nakupuji zde poprvé a nemám učet
                                        </span>
                                    </h4>
                                </div>
                                <form>
                                    <div class="input-group">
                                        <label>
                                            váš email *
                                        </label>
                                        <input type="text" class="form-control"/>
                                    </div>
                                    <span class="legend">*povinná položka</span>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="read-more-blog login-button register-button"><a href="#">Registrovat</a></div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <a href="#" class="without-registration">Nákup bez registrace</a>
                                        </div>
                                    </div>
                                </form>
                                <br/>
<style>label {
    font-weight: normal;
}</style>
                                <div class="hl-title cart-title">
                                    <h3>
                                        <span>Proč se registrovat?</span>
                                    </h3>
                                    <span class="registration-reason">
                                        1. orem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </span>
                                    <br/>
                                    <span class="registration-reason">
                                        2. orem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </span>
                                    <br/>
                                    <span class="registration-reason">
                                        3. orem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </span>
                                    <br/>
                                    <span class="registration-reason">
                                        4. orem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </span>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row cart-footer">
                            <div class="col-md-6">
                                <p>
                                <span class="description">
                                    Celkem:
                                </span>
                                    <span class="content">
                                    2 položky
                                </span>
                                </p>
                            </div>
                            <div class="col-md-6 " style="display: block; margn-left: auto; margin-right: auto; text-align: right">
                                <p>
                                <span class="description">
                                    Cena zboží:
                                </span>
                                    <span class="content">
                                    3 580 KČ
                                </span>
                                    <span class="note">
                                    (s DPH bez dopravy )
                                </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="third_step step">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">

                                <h4 style="color:#f39800">
                                    Doprava
                                </h4>

                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="optionsRadios1" id="optionsRadios1" value="option1" checked>
                                                        osobní odběr
                                                        <span class="price">ZDARMA</span>
                                                        <br/>
                                                        <span class="note">adresa skladu</span>
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="optionsRadios1" id="optionsRadios2" value="option2">
                                                        PPL &nbsp;<span class="price">+90KČ</span>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>



                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">

                                <h4 style="color:#f39800">
                                    Platba
                                </h4>

                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="optionsRadios2" id="optionsRadios1" value="option1" checked>
                                                        dobírka<span class="price">+60KČ</span>
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="optionsRadios2" id="optionsRadios2" value="option2">
                                                        kartou online &nbsp;<span class="price">ZDARMA</span>
                                                    </label>
                                                </div>
                                            </div>

                                                                                    </div>
                                    </div>



                                    <div class="image_box" >
                                        <img src="assets/images/1.png" />
                                        <img src="assets/images/2.png" />
                                        <img src="assets/images/3.png" />
                                        <img src="assets/images/5.png" />
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row cart-footer">

                            <div class="col-md-12 col-sm-12 col-xs-12 " style="display: block; margin-left: auto; margin-right: auto; text-align: left">
                                <br/>
                                <p style="text-align: left">
                                    <span style="color:rgba(0,0,0,.4); font-weight: bold">Cena za položky</span>
                                    <span style="color:rgba(0,0,0,.4);">3430 KČ</span>
                                </p>
                                <p style="text-align: left">
                                    <span style="color:rgba(0,0,0,.4); font-weight: bold">Cena za položky</span>
                                    <span style="color:rgba(0,0,0,.4);">3430 KČ</span>
                                </p>
                                <p style="text-align: left">
                                    <span style="color:rgba(0,0,0,.4); font-weight: bold">Cena za položky</span>
                                    <span style="color:rgba(0,0,0,.4);">3430 KČ</span>
                                </p>
                                <p style="text-align: left">
                                    <span style="color:rgba(0,0,0,.4); font-weight: bold">Cena za položky</span>
                                    <span style="color:rgba(0,0,0,.4);">3430 KČ</span>
                                </p>
                                <p style="text-align: left">
                                    <span style="color:black; font-weight: bold">Cena celkem s DPH</span>
                                    <span style="color:#f39800">3430 KČ</span>
                                </p>
                                <div class="read-more-blog login-button" style="width:auto; float: left"><a href="#">Dokončit objednávku</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fourth_step step">
            <div class="container">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-12">
                        <div class="cart">
                            <table>
                                <tr>
                                    <td style="width:150px">
                                        <img src="assets/images/produkt1.png" style="width:100%"/>
                                    </td>
                                    <td>
                                        <span class="product_name">Opera 8,6 urlcore</span>
                                        <br/>
                                        <span class="brand">Beal</span>
                                    </td>
                                    <td>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                            <span class="description" >Cena za kus
                                            <br/>
                                                1780KČ
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                                <span class="description" >Množství </span>
                                                <span class="content">
                                           &nbsp; 1KS
                                        </span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                                <span class="input_description" >Dostupnost</span>
                                                <span class="input" style="width:50%;padding-top:2%">
                                                &nbsp;skladem<i class="fa fa-check" style="color:green"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row">

                                            <div class="price">
                                                <span class="price">1790 KČ</span>

                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:150px">
                                        <img src="assets/images/produkt1.png" style="width:100%"/>
                                    </td>
                                    <td>
                                        <span class="product_name">Opera 8,6 urlcore</span>
                                        <br/>
                                        <span class="brand">Beal</span>
                                    </td>
                                    <td>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                            <span class="description">Cena za kus
                                            <br/>

                                                1780KČ
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                                <span class="description" >Množství</span>
                                                <span class="content">
                                           &nbsp; 1KS
                                        </span>

                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div>
                                                    <span class="input_description" >Dostupnost</span>
                                                    <span class="input" style="width:50%;padding-top:2%">
                                               &nbsp; skladem<i class="fa fa-check" style="color:green"></i>
                                            </span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>


                                    <td>
                                        <div class="row">

                                            <div class="price">
                                                <span class="price">1790 KČ</span>

                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <br/>
                                    <p style="text-align: left">
                                        <span style="color:black; font-weight: bold">Celkem:</span>
                                        <span style="color:#f39800">2 položky</span>
                                    </p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <br/>
                                    <p style="text-align: center">
                                        <span style="color:rgba(0,0,0,.4); font-weight: bold">Cena za položky</span>
                                        <span style="color:rgba(0,0,0,.4);">3430 KČ</span>
                                    </p>
                                    <p style="text-align: center">
                                        <span style="color:rgba(0,0,0,.4); font-weight: bold">Cena za položky</span>
                                        <span style="color:rgba(0,0,0,.4);">3430 KČ</span>
                                    </p>
                                    <p style="text-align: center">
                                        <span style="color:rgba(0,0,0,.4); font-weight: bold">Cena za položky</span>
                                        <span style="color:rgba(0,0,0,.4);">3430 KČ</span>
                                    </p>
                                    <p style="text-align: center">
                                        <span style="color:rgba(0,0,0,.4); font-weight: bold">Cena za položky</span>
                                        <span style="color:rgba(0,0,0,.4);">3430 KČ</span>
                                    </p>
                                    <p style="text-align: center">
                                        <span style="color:black; font-weight: bold">Cena celkem s DPH</span>
                                        <span style="color:#f39800">3430 KČ</span>
                                    </p>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12"></div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Souhlasím s obchodními podmínkami
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Chci zasílat informace o novinkách
                                        </label>
                                    </div>
                                    <div class="read-more-blog login-button" style="width:300px; float: none"><a href="#">Objednat</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.step_link').click(function(){

            //set active step
            var id = $(this).attr('id');
            var loader = $('.loader');

            loader.attr('id', id);

            $('.step').removeClass('active_step').fadeOut('fast');
            $('.step.'+id).addClass('active_step').fadeIn('fast');


            $('.step').removeClass('active_step').fadeOut('fast');
            $('.step.'+id).addClass('active_step').fadeIn('fast');

            var current_element = $(this).parent();
            var parent = current_element.parent()

            parent.find('.stage').removeClass('active-line').removeClass('active')
            current_element.addClass('active-line').addClass('active');

            var current_index = $('.stage').index(current_element);

            for(var i =  0; i < current_index;i++ ){

                $(parent.find('.stage')[i]).addClass('active-line');
            }


        })
        $('.without-registration').click(function(){

            $('.login-register').css('display','none');
            $('.without-registration-content').css('display','block')
        })
    })
</script>