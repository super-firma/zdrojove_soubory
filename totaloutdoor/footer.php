</body>
<footer class="footer">
	
	<div class="row">
		<div class="container">
		<div class="bs-col-container widgety">
			<div class="bs-col5 widgety2">
				<div class="logo-footer"><img class="logo-footer-obrazok" src="../assets/images/logo_footer.png"/><br />
					<div class="adresa">
						<p>Horní Rokytnice 470 <br />
						512 44 Rokytnice nad Jizerou</p>
					</div>
					<div class="email">
						<a target="_blank" href="mailto:info@totaloutdoor.cz"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@totaloutdoor.cz</a>

					</div>
				
				</div>
			</div>
			<div class="bs-col5 widgety2">
				<h4>MENU</h4>
					<ul class="footer-1">
						<li class="footer-1-menu"><a target="_blank" href="../kdo-jsme.php">kdo jsme?</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../nase-znacky.php">naše značky</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../category.php">katalog produktu</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../prodejci.php">prodejci</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../blog.php">blog</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../contact.php">kontakt</a></li>
					</ul>
				</div>		
			<div class="bs-col5 widgety2">
				<h4>SEKCE</h4>
					<ul class="footer-1">
						<li class="footer-1-menu"><a target="_blank" href="../horolezectvi">horolezectví</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../prace-ve-vyskach">práce ve výškách</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../detske-vybaveni">detske vybavení</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../camping">camping</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../pro-zvirata">pro zvirata</a></li>
					</ul></div>
			<div class="bs-col5 widgety2">
				<h4>VŠE O NÁKUPU</h4>
					<ul class="footer-1">
						<li class="footer-1-menu"><a target="_blank" href="../obchodni-podminky">obchodní podmínky</a></li>
						<li class="footer-1-menu"><a target="_blank" href="../vymena-vraceni-zbozi">vymnena/vrácení zboží</a></li>
					</ul>
					<h4>SOCIÁLNÍ SÍTE</h4>
					<ul class="footer-1">
						<li class="footer-1-socialne-site"><a target="_blank" href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
						<li class="footer-1-socialne-site"><a target="_blank" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li class="footer-1-socialne-site"><a target="_blank" href="#"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a></li>
					</ul>
			</div>
			<div class="bs-col5 widgety2">
				<h4>PRO PARTNERY</h4>
				<h4>MAPA STRÁNEK</h4>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-6">
                        <div class="copyright">
                            <p>© 2017 TOTAL outdoor<br />All Rights reserved.</p>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-6">
                        <img class="copyright-logo" src="../assets/images/copyright-logo.png"/>
                    </div>
                </div>
	</div>

</div></div>
</footer>