<!DOCTYPE php>
<php lang="cs" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/styles.css?v=1.1.5" type="text/css" media="screen">
    <title>Jaromír Nigrin</title>
    <meta name="description" content="Zahradnické služby">
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet"  href="libs/lightslider-master/src/css/lightslider.css"/>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
    <script src="libs/lightslider-master/src/js/lightslider.js"></script>
    <script>
    	 $(document).ready(function() {
			$("#content-slider").lightSlider({
                loop:true,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }
            });
		});
    </script>
    <script type = "text/javascript">
    $(function () {
        var body = $('#slider');
        var backgrounds = [
          'url(images/titulky/titulka_01.jpg)',
          'url(images/titulky/titulka_02.jpg)',
          'url(images/titulky/titulka_03.jpg)',
          'url(images/titulky/titulka_04.jpg)',
          'url(images/titulky/titulka_05.jpg)',
          'url(images/titulky/titulka_06.jpg)',
          'url(images/titulky/titulka_07.jpg)',
          'url(images/titulky/titulka_08.jpg)',
          'url(images/titulky/titulka_09.jpg)',
          'url(images/titulky/titulka_10.jpg)'];
        var current = 0;

        function nextBackground() {
            body.css(
                'background-image',
            backgrounds[current = ++current % backgrounds.length]);

            setTimeout(nextBackground, 10000);
        }
        setTimeout(nextBackground, 10000);
        body.css('background-image', backgrounds[0]);
    });
    </script>
    <script src="functions/functions.js"></script>
  </head>

  <body>
    <header>
      <div class="content">
        <a href="index.php"><img src="images/logo.png" alt="Zahradnické služby Jaromír Nigrin" id="logo"></a>
        <h1>Zahradnické služby Jaromír Nigrin</h1>
        <ul id="menu">
          <li><a href="index.php">Úvod</a></li>
          <li><a href="index.php#sluzby">Naše služby</a></li>
          <li><a href="galerie.php">Reference</a></li>
          <li><a href="index.php#o-nas">O nás</a></li>
          <li><a href="index.php#contact">Kontakt</a></li>
          <li class="dark"><span><img src="images/telefon.png" alt="Telefon">+420 608 272 503</span></li>
        </ul>
        <button id="menu-button" type="button">≡</button>
      </div>
    </header>

    <div id="main" class="content">
      <div class="item">
          <div class="clearfix" style="max-width:474px;">
              <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                <?php
                $files = scandir('images/reference/mini/');
                if ($files !== false)
                {
                   foreach((array)$files as $f) {
                     if ($f == '..' || $f == '.')
                         continue;
                     echo '<li data-thumb="images/reference/mini/'.$f.'"><div style="background-image:url(images/reference/'.$f.');background-size:cover;width:100%;height:611px;"></div><img src="images/reference/'.$f.'" alt="Naše reference" style="display:none;"></li>'."\n";
                     echo ''."\n";
                   }
                }
                ?>
              </ul>
          </div>
      </div>
    </div>

    <footer>
      &copy; Copyright 2017 Zahradnické služby Jaromír Nigrin
    </footer>

  </body>
