<!DOCTYPE php>
<php lang="cs" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/styles.css?v=1.1.5" type="text/css" media="screen">
    <title>Jaromír Nigrin</title>
    <meta name="description" content="Zahradnické služby">
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet"  href="libs/lightslider-master/src/css/lightslider.css"/>
    <style>
    .lSPager.lSGallery{display:none;}
    </style>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
    <script src="libs/lightslider-master/src/js/lightslider.js"></script>
    <link rel="stylesheet" href="libs/colorbox/colorbox.css">
    <script src="libs/colorbox/jquery.colorbox.js"></script>
    <script>
      $(document).ready(function(){
        $(".group1").colorbox({rel:'group1', maxWidth:'96%', maxHeight:'96%'});

        $("#click").click(function(){
          $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
          return false;
        });
      });
    </script>
    <script>
    	 $(document).ready(function() {
			$("#content-slider").lightSlider({
                loop:true,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }
            });
		});
    </script>
    <script type = "text/javascript">
    $(function () {
        var body = $('#slider');
        var backgrounds = [
          'url(images/titulky/titulka_01.jpg)',
          'url(images/titulky/titulka_02.jpg)',
          'url(images/titulky/titulka_03.jpg)',
          'url(images/titulky/titulka_04.jpg)',
          'url(images/titulky/titulka_05.jpg)',
          'url(images/titulky/titulka_06.jpg)',
          'url(images/titulky/titulka_07.jpg)',
          'url(images/titulky/titulka_08.jpg)',
          'url(images/titulky/titulka_09.jpg)',
          'url(images/titulky/titulka_10.jpg)'];
        var current = 0;

        function nextBackground() {
            body.css(
                'background-image',
            backgrounds[current = ++current % backgrounds.length]);

            setTimeout(nextBackground, 20000);
        }
        setTimeout(nextBackground, 20000);
        body.css('background-image', backgrounds[0]);
    });
    </script>
    <script src="functions/functions.js"></script>
  </head>

  <body>
    <header>
      <div class="content">
        <a href="index.php"><img src="images/logo.png" alt="Zahradnické služby Jaromír Nigrin" id="logo"></a>
        <h1>Zahradnické služby Jaromír Nigrin</h1>
        <ul id="menu">
          <li><a href="index.php">Úvod</a></li>
          <li><a href="index.php#sluzby">Naše služby</a></li>
          <li><a href="galerie.php">Reference</a></li>
          <li><a href="index.php#o-nas">O nás</a></li>
          <li><a href="index.php#contact">Kontakt</a></li>
          <li class="dark"><span><img src="images/telefon.png" alt="Telefon">+420 608 272 503</span></li>
        </ul>
        <button id="menu-button" type="button">≡</button>
      </div>
    </header>

    <div id="main" class="content">
      <div id="slider"></div>
      <h2 id="sluzby" class="twolines"><span>Poskytujeme komplexní zahradnické služby, návrhy, realizace a základní arboristické práce</span></h2>
      <div class="info">
        <span class="dark">Návrhy, realizace a údržba zeleně</span>
        Výsadba vzrostlých stromů
        <span class="dark">Arboristika - komplexní péče o dřeviny</span>
        Kácení stromů stromolezeckou technikou
        <span class="dark">Profesionální údržba trávníků</span>
        Automatické závlahové systémy <img src="images/hunter.png" alt="Hunter">
        <span class="dark">Koupaci jezírka a BIO bazény</span>
      </div>

      <h2>Výběr z našich realizací</h2>
      <div id="gallery">
        <a href="images/galerie/1.jpg" class="group1"><span>Zahrada roku 2015<br>Praha 4, Lhotka</span><img src="images/galerie/mini/1.jpg"></a>
        <a href="images/galerie/2.jpg" class="group1"><span>Zahrada roku 2015<br>Praha 4, Lhotka</span><img src="images/galerie/mini/2.jpg"></a>
        <a href="images/galerie/3.jpg" class="group1"><span>Zahrada roku 2015<br>Praha 4, Lhotka</span><img src="images/galerie/mini/3.jpg"></a>
        <a href="images/galerie/4.jpg" class="group1"><span>Zahrada roku 2015<br>Praha 4, Lhotka</span><img src="images/galerie/mini/4.jpg"></a>
        <a href="images/galerie/5.jpg" class="group1"><span>Zahrada roku 2015<br>Praha 4, Lhotka</span><img src="images/galerie/mini/5.jpg"></a>
        <a href="images/galerie/6.jpg" class="group1"><span>Zahrada roku 2015<br>Praha 4, Lhotka</span><img src="images/galerie/mini/6.jpg"></a>
        <a href="images/galerie/1.jpg" class="group1"><span>Zahrada roku 2015<br>Praha 4, Lhotka</span><img src="images/galerie/mini/1.jpg"></a>
        <a href="images/galerie/2.jpg" class="group1"><span>Zahrada roku 2015<br>Praha 4, Lhotka</span><img src="images/galerie/mini/2.jpg"></a>
        <a href="images/galerie/3.jpg" class="group1"><span>Zahrada roku 2015<br>Praha 4, Lhotka</span><img src="images/galerie/mini/3.jpg"></a>
      </div>

      <h2 id="o-nas">Jaromír Nigrin - Zahradnické služby</h2>
      <div class="item">
          <div class="clearfix" style="max-width:474px;">
              <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                <?php
                $files = scandir('images/slideshow/');
                if ($files !== false)
                {
                   foreach((array)$files as $f) {
                     if ($f == '..' || $f == '.')
                         continue;
                     echo '<li data-thumb="images/slideshow/'.$f.'"><div style="background-image:url(images/slideshow/'.$f.');background-size:cover;width:100%;height:611px;"></div><img src="images/slideshow/'.$f.'" alt="Naše reference" style="display:none;"></li>'."\n";
                     echo ''."\n";
                   }
                }
                ?>
              </ul>
          </div>
      </div>

      <h3>Ocenění</h3>
      <div class="oceneni">
        <div class="box">
          <a href="images/oceneni/diplom_01.jpg"><img src="images/oceneni/diplom_01.jpg" alt="Diplom"></a>
          <a href="images/oceneni/diplom_02.jpg"><img src="images/oceneni/diplom_02.jpg" alt="Diplom"></a>
        </div>
      </div>

      <h3>Členství v oborových sdruženích</h3>
      <div id="clenstvi">
        <img src="images/loga/szuz.jpg">
        <img src="images/loga/szkt.jpg">
      </div>

      <h3>Podporujeme</h3>
      <div id="podporujeme">
        <img src="images/podporujeme/1.png">
        <img src="images/podporujeme/2.png">
        <img src="images/podporujeme/3.png">
        <img src="images/podporujeme/4.png">
      </div>
      <h3>Spolupracujeme</h3>
      <div id="podporujeme">
        <img src="images/spolupracujeme/1.png">
        <img src="images/spolupracujeme/2.png">
        <img src="images/spolupracujeme/3.png">
      </div>
      <h2 class="res">Kontaktní informace a poptávkový formulář</h2>
      <div id="contact-img"></div>
      <div id="contact">
        <div class="contact">
          <div class="nadpis">Kontaktní informace:</div>
          <ul class="dark">
            <li>Jaromír Nigrin</li>
            <li>Tel.: +420 608 272 503</li>
            <li>E-mail: info@ngrin.cz</li>
            <li>www.nigrin.cz</li>
          </ul>

          Sídlo:
          <ul>
            <li>Kunešová 2653/18</li>
            <li>Praha 3, 130 00</li>
          </ul>

          Pobočka:
          <ul>
            <li>Mokrá Lhota 28</li>
            <li>Bystřice, 257 28</li>
          </ul>
        </div>
        <div class="contact-form">
          <div class="nadpis">Máte jakýkoliv dotaz? Napište nám nebo zavolejte.</div>
          <?php include "functions/contact-form.php"; ?>
        </div>
        <div class="clear"></div>
      </div>
    </div>

    <footer>
      &copy; Copyright 2017 Zahradnické služby Jaromír Nigrin
    </footer>

  </body>
