<?php
if (preg_match("/kontaktni-formular.php/i", $_SERVER['PHP_SELF'])) {
  die();
}
?>
<form name="kontaktni-formular" method="post">
  <input name="jmeno" type="text" placeholder="Jméno a příjmení" class="input" required>
  <input name="email" type="text" placeholder="E-mail" class="input" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" required>
  <input name="telefon" type="text" placeholder="Telefon" class="input" required>
  <textarea name="text" placeholder="Vaše představa" class="input" required></textarea>
  <input type="submit" value="Odeslat" class="button">
</form>
<?php
  $odeslat = 0;
  if(isset($_POST['email'])) {
    if(!isset($_POST['email']) || !isset($_POST['text'])) {
?>
    Je nám líto, ale zdá se, že kontaktní formulář přestal fungovat.
<?php
    } else {
      $odeslat = 1;
    }

    $email_reply = $_POST["email"];
    $jmeno = $_POST["jmeno"];
    $telefon = $_POST["telefon"];
    $text = $_POST["text"];

    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
    if(!preg_match($email_exp, $email_reply)) {
      $odeslat = 0;
?>
      Omlouváme se, ale formulář se nepodařilo odeslat.<br>
      Zadaná emailová adresa se nezdá být správná.
<?php
    }


    if($odeslat == 1) {
      $email_from = "info@ngrin.cz";
      $email_to = "info@ngrin.cz";
      $email_subject = "Kontaktní formulář - Jaromír Nigrin";
      $email_message = "<html><body>";
      $email_message .= "<h1 style='margin-top:7px;font-size:28px;font-weight:normal;'>Kontaktní formulář - Jaromír Nigrin</h1>";

      function clean_string($string) {
        $bad = array("content-type","bcc:","to:","cc:","href");
        return str_replace($bad,"",$string);
      }

      $email_message .= "<div style='padding:5px 25px;margin:0 20px 10px 20px;border:1px solid #b8c0c5;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;'>";
      $email_message .= "<h2 style='font-size:14px;display:inline-block;'>E-mail: </h2>";
      $email_message .= " ".strip_tags($email_reply);
      $email_message .= "</div>";

      $email_message .= "<div style='padding:5px 25px;margin:0 20px 11px 20px;border:1px solid #b8c0c5;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;'>";
      $email_message .= "<h2 style='font-size:14px;display:inline-block;'>Jméno: </h2>";
      $email_message .= " ".strip_tags($jmeno);
      $email_message .= "</div>";

      $email_message .= "<div style='padding:5px 25px;margin:0 20px 12px 20px;border:1px solid #b8c0c5;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;'>";
      $email_message .= "<h2 style='font-size:14px;display:inline-block;'>Telefon: </h2>";
      $email_message .= " ".strip_tags($telefon);
      $email_message .= "</div>";

      $email_message .= "<div style='padding:5px 25px;margin:0 20px 13px 20px;border:1px solid #b8c0c5;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;'>";
      $email_message .= "<h2 style='font-size:14px;display:inline-block;'>Zpráva: </h2>";
      $email_message .= "<p>".strip_tags($text)."</p>";
      $email_message .= "</div>";

      $email_message .= "</body></html>";

      // create email headers
      $headers = "MIME-Version: 1.0\n";
      $headers .= "Content-Type: text/html; charset=UTF-8\n";
      $headers .= "Content-Transfer-Encoding: 8bit\n";
      $headers .= "From: ".strip_tags($email_from)."\r\n".
      "Reply-To: ".strip_tags($email_reply)."\r\n" .
      "X-Mailer: PHP/".phpversion();
      mail($email_to, $email_subject, $email_message, $headers);
?>
      Formulář byl úspěšně odeslán.
<?php
      $current_page = curPageURL();
      header("Location: ".$current_page."?odeslano");
    }
  }
?>
