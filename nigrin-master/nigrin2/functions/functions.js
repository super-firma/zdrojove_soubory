$(document).ready(function() {

    $(window).resize(function() {
      if ($(window).width() > 1100) {
        $("#menu").css("display", "block");
      } else {
        $("#menu").css("display", "none");
      }
    });

    $("#menu-button").click(function() {
      $("#menu").slideToggle(200);
    });

});
